# Python Program for addition of two number

# Declare the number
v_num1 = int(input("Enter first number : "))
v_num2 = int(input("Enter second number : "))

# Make the addition of input taken number
v_addition = v_num1 + v_num2

# Print the number
print(f"Addition of two number is: {v_num1} + {v_num2} = {v_addition}")
