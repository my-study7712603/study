from flask import Flask, render_template, request, redirect, url_for
import psycopg2
app = Flask(__name__)
GENDER_PLACEHOLDER: str = 'Select Gender'
db_params = {
    'host': 'localhost',
    'database': 'form_data',
    'user': 'postgres',
    'password': 'Ashish@1996',
}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        dob = request.form['dob']
        address = request.form['address']
        pincode = str(request.form['pincode'])
        nominee = str(request.form['nominee'])
        try:
            gender = str(
                request.form['gender'] if request.form['gender'] != GENDER_PLACEHOLDER else None)
        except Exception:
            gender = None
        if not name or not email:
            return render_template('error.html', message='Name and Email are required.')
        if not phone:
            phone = ''
        if not dob:
            # return render_template('error_dob.html', message='Date Of Birth is required. Please try again.')
            dob = None
        if not address:
            address = ''
        if not pincode:
            pincode = ''
        if not gender:
            nominee = 'Nominee Not Available'
            if not nominee or nominee == 'Nominee Not Available':
                gender = ''
        conn = psycopg2.connect(**db_params)
        cur = conn.cursor()
        cur.execute(
            "INSERT INTO user_info (name, email, phone, dob, address, pincode, nominee_name, nominee_gender) VALUES ("
            "%s, %s, %s, %s,%s, %s, %s, %s)",
            (name, email, phone, dob, address, pincode, nominee, gender))
        conn.commit()
        cur.close()
        conn.close()
        return render_template('success.html', name=name, email=email, phone=phone, dob=dob, address=address)


@app.route('/search')
def search():
    query = request.args.get('query')
    if query:
        conn = psycopg2.connect(**db_params)
        cur = conn.cursor()
        if '@' in query:
            cur.execute("SELECT * FROM user_info WHERE email = %s", (query,))
        else:
            cur.execute("SELECT * FROM user_info WHERE phone = %s", (query,))
        entries = cur.fetchall()
        cur.close()
        conn.close()
        return render_template('search_results.html', entries=entries, query=query)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)

# Code End