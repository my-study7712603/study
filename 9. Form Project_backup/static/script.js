document.addEventListener('DOMContentLoaded', function () {
    const form = document.querySelector('form');

    form.addEventListener('submit', function (event) {
        const nameInput = document.getElementById('name');
        const emailInput = document.getElementById('email');
        const phoneInput = document.getElementById('phone');
        const dobInput = document.getElementById('dob');
        const searchInput = document.getElementById('searchInput');  // Add this line


        if (!validateName(nameInput.value)) {
            alert('Please enter a valid name.');
            event.preventDefault();
        }

        if (!validateEmail(emailInput.value)) {
            alert('Please enter a valid email address.');
            event.preventDefault();
        }

        if (!validatePhone(phoneInput.value)) {
            alert('Please enter a valid phone number.');
            event.preventDefault();
        }

        // Validate Date of Birth
        if (dobInput.value && !validateDate(dobInput.value)) {
            alert('Please enter a valid Date of Birth.');
            event.preventDefault();
        }
    });

    function validateName(name) {
        return /^[a-zA-Z\s]+$/.test(name);
    }

    function validateEmail(email) {
        return /\S+@\S+\.\S+/.test(email);
    }

    function validatePhone(phone) {
        return /^\d{10}$/.test(phone);
    }

    function validateDate(date) {
        const regex = /^\d{4}-\d{2}-\d{2}$/;
        return regex.test(date);
    }

    function searchEntries() {
    const searchInput = document.getElementById('searchInput').value;

    // Validate search input (you can add more validation as needed)
    if (searchInput.trim() !== '') {
        // Redirect to search route with the provided input
        window.location.href = `/search?query=${encodeURIComponent(searchInput)}`;
    } else {
        alert('Please enter a Mobile Number or Email to search.');
    }
}
});



